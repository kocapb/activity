<?php

namespace App\DTO;

use Spatie\DataTransferObject\DataTransferObject;

/**
 * Class ActivityDTO
 * @package App\DTO
 */
class ActivityDTO extends DataTransferObject
{
    public string $url;
    public string $date;
}
