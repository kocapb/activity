<?php


namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Activity
 * @package App\Models
 *
 * @property string $url
 * @property string $total
 * @property Carbon $date
 */
class Activity extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'url',
        'date',
        'total',
    ];

    protected $casts = [
        'date' => 'datetime'
    ];
}
