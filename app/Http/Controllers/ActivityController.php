<?php

namespace App\Http\Controllers;

use App\DTO\ActivityDTO;
use App\Http\Requests\ActivityRequest;
use App\Jobs\ActivitySetter;
use App\Models\Activity;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Bus;

/**
 * Class ActivityController
 * @package App\Http\Controllers
 */
class ActivityController extends Controller
{
    /**
     * @param ActivityRequest $request
     * @return JsonResponse
     */
    public function __invoke(ActivityRequest $request): JsonResponse
    {

        Bus::dispatch(new ActivitySetter(new ActivityDTO($request->validated())));

        return response()->json(['success' => true]);
    }
}
