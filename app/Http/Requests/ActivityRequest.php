<?php


namespace App\Http\Requests;

/**
 * Class ActivityRequest
 * @package App\Http\Requests
 *
 * @property string $url
 * @property string $date
 */
class ActivityRequest extends BaseRequest
{
    public function rules(): array
    {
        return [
            'url' => 'required|url',
            'date' => 'required|date_format:Y-m-d H:i:s'
        ];
    }
}
